/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dospasos;

import java.net.*;
import java.io.*;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;

public class MultiServerThread extends Thread {
   private Socket socket = null;

   public MultiServerThread(Socket socket) {
      super("MultiServerThread");
      this.socket = socket;
      DosPasos.NoClients++;
   }

   public void run() {

      try {
         final PrintWriter escritor = new PrintWriter(socket.getOutputStream(), true);
         BufferedReader entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
         String lineIn, lineOut;
		
	     while((lineIn = entrada.readLine()) != null){
            System.out.println("Received: "+lineIn);
            escritor.flush();
            if(lineIn.equals("FIN")){
               DosPasos.NoClients--;
			      break;
			   }else{
                
                //Valida el protocolo Inicio y fin:
                if(lineIn.startsWith("#") && lineIn.endsWith("&")){
                int tam= lineIn.length();
                
                String  DR= lineIn.toString();
                        String pc;
                        pc = DR.substring(3,8);
                        System.out.println("Empieza con:"+DR.charAt(1)+"pc:"+pc);
                            //Inicia validación del protocolo de cifrado
                            if(DR.charAt(1)=='I' && pc.equalsIgnoreCase("clave")){
                                
                                //Validación de contraseña
                                int e=9;
                                String Llave="";
                                 do{
                                    Llave=Llave+DR.charAt(e);
                                    e++;                                  
                                 }while(DR.charAt(e)!='&');
                                 System.out.println("Llave:"+Llave);
                                
                                
                                if(Llave.equals("password")){
                                    
                                    //Empieza el timer
                                    Timer timer= new Timer();
                                    TimerTask task= new TimerTask(){

                                        @Override
                                        public void run() {
                                            System.out.println("Estoy publicando cada minuto");
                                            //Empieza a genera el codigo aleatoria
                                    Random aleatorio = new Random();
                                    String alfa = "ABCDEFGHIJKLMNOPQRSTVWXYZ";
                                    String codigo = "";    //Inicializamos la Variable//
                                    int numero;
                                    int forma;
                                    forma=(int)(aleatorio.nextDouble() * alfa.length()-1+0);
                                    numero=(int)(aleatorio.nextDouble() * 99+100);
                                    codigo=codigo+alfa.charAt(forma)+numero;
                                    System.out.println("El codigo generado es:"+codigo);
                                    //Fin de generación del código
                                    escritor.println("#2P|Clave|"+codigo+"&");
                                    escritor.flush();//se envia respuesta
                                    //Termina el cifrado
                                    //Se publica la info en fb
                                     String r="Nuevo codigo:"+codigo;

                                    HttpResponse re;
                                    String accessToken = "EAAlkOYBQB4QBAGUOhoyuTGoOSvwkIqTZCuvNWriERF2vzAW7PakeXzjZBUuPAlEC0zcRvZC5ipFzKzxLfDZASn9NuCORrSh1nAlZA4EdawmkxWUULElXT170Kn2FotPqjCnTdVTfPZBPW2LQdYGGWCXDxOLXaN2z4os2daefclZAhIkQE8LT3VpWfOeG7FQdTAZD";
                                            try {
                                                re = Request.Post("https://graph.facebook.com/105312894202306/feed/").bodyForm(Form.form().add("message",r).add("access_token", accessToken).build()).execute().returnResponse();
                                            } catch (IOException ex) {
                                                Logger.getLogger(MultiServerThread.class.getName()).log(Level.SEVERE, null, ex);
                                            }
            
                                    System.out.println("Se ha publicado el nuevo código con éxito");
                                    
                                    //Fin de la publicación
                                    
                                        }
                                    
                                    
                                    };
                                    timer.schedule(task, 10, 60000);
                                    //Finaliza el timer
                                    
                                    
                                }
                                else{
                                    escritor.println("Echo... Contraseña incorrecta");
                                    escritor.flush();
                                }
                            }
                            //Fin del protocolo de cifrado...
                            
                            else{
                            escritor.println("Error, servicio no disponible.");
                            escritor.flush();
                            }//Validacion de codigo de servicio
                
                }
                else{
                            escritor.println("Echo..Los datos enviados NO cumplen con el protocolo.");
                            escritor.flush();
                        }//Fin de la validación del protocolo Inicio y fin.
                
               
                
               
            }
         } 
         try{		
            entrada.close();
            escritor.close();
            socket.close();
         }catch(Exception e){ 
            System.out.println ("Error : " + e.toString()); 
            socket.close();
            System.exit (0); 
   	   } 
      }catch (IOException e) {
         e.printStackTrace();
      }
   }
} 