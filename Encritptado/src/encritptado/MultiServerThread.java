/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package encritptado;

import java.net.*;
import java.io.*;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

public class MultiServerThread extends Thread {
   private Socket socket = null;

   public MultiServerThread(Socket socket) {
      super("MultiServerThread");
      this.socket = socket;
      Encritptado.NoClients++;
   }

   public void run() {

      try {
         PrintWriter escritor = new PrintWriter(socket.getOutputStream(), true);
         BufferedReader entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
         String lineIn, lineOut;
		
	     while((lineIn = entrada.readLine()) != null){
            System.out.println("Received: "+lineIn);
            escritor.flush();
            if(lineIn.equals("FIN")){
               Encritptado.NoClients--;
			      break;
			   }else{
                
                //Valida el protocolo Inicio y fin:
                if(lineIn.startsWith("#") && lineIn.endsWith("&")){
                int tam= lineIn.length();
                
                char [] DR= lineIn.toCharArray();
                            //Validacion del codigo de servicio
                        
                            //Inicia validación del protocolo de cifrado
                            if(DR[1]=='C' && DR[2]=='R' && DR[3]=='Y'){
                                if(DR[5]=='2'){
                                    int e=7;
                                    String Llave="";
                                    do{
                                        Llave=Llave+DR[e];
                                        e++;                                  
                                    }while(DR[e]!='|');
                                    System.out.println("Llave:"+Llave);
                                    e++;
                                    String cadena="";
                                    do{
                                        cadena=cadena+DR[e];
                                        e++;                                  
                                    }while(DR[e]!='&');
                                    System.out.println("Cadena:"+cadena);
                                    
                                    //Empieza el cifrado
                                    SecretKeySpec skeyspec=new SecretKeySpec(Llave.getBytes(),"Blowfish");
                                    Cipher cipher=Cipher.getInstance("Blowfish");
                                    cipher.init(Cipher.ENCRYPT_MODE, skeyspec);
                                    byte[] encrypted=cipher.doFinal(cadena.getBytes());
                                    String strData=new String(encrypted);
                                    System.out.println(strData);
                                    escritor.println("#R-CRY|1|"+strData+"&");
                                    escritor.flush();//se envia respuesta
                                    //Termina el cifrado
                                }
                                else{
                                    escritor.println("Echo... Los datos enviados NO cumplen con el protocolo");
                                    escritor.flush();
                                }
                            }
                            //Fin del protocolo de cifrado...
                            
                            else{
                            escritor.println("Error, servicio no disponible.");
                            escritor.flush();
                            }//Validacion de codigo de servicio
                
                }
                else{
                            escritor.println("Echo..Los datos enviados NO cumplen con el protocolo.");
                            escritor.flush();
                        }//Fin de la validación del protocolo Inicio y fin.
                
               
                
               
            }
         } 
         try{		
            entrada.close();
            escritor.close();
            socket.close();
         }catch(Exception e){ 
            System.out.println ("Error : " + e.toString()); 
            socket.close();
            System.exit (0); 
   	   } 
      }catch (IOException e) {
         e.printStackTrace();
      } catch (NoSuchAlgorithmException ex) {
           Logger.getLogger(MultiServerThread.class.getName()).log(Level.SEVERE, null, ex);
       } catch (NoSuchPaddingException ex) {
           Logger.getLogger(MultiServerThread.class.getName()).log(Level.SEVERE, null, ex);
       } catch (InvalidKeyException ex) {
           Logger.getLogger(MultiServerThread.class.getName()).log(Level.SEVERE, null, ex);
       } catch (IllegalBlockSizeException ex) {
           Logger.getLogger(MultiServerThread.class.getName()).log(Level.SEVERE, null, ex);
       } catch (BadPaddingException ex) {
           Logger.getLogger(MultiServerThread.class.getName()).log(Level.SEVERE, null, ex);
       }
   }
} 
