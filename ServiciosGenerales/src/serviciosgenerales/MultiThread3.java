import java.net.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.security.MessageDigest;
import java.util.Arrays;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;
//import com.restfb.*;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;
import org.apache.http.*;
import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class MultiThread3 extends Thread{
    private Socket socket = null;
    boolean servidor1 = false, servidor2 = false, servidor3 = false;
    int servicioofrecido=0;
    
    public void ChecaSiOfrece(String entrada){
        int i=0;
        String clave="";
        clave=entrada.substring(0,3)+entrada.substring(entrada.length()-1,entrada.length());
        ServiciosGenerales servgen = new ServiciosGenerales();
        Servidor3 serv3 = new Servidor3();
        
        for (i=0; i<=11; i++){
            if (serv3.LSL[i][2].equals(clave) && serv3.LSL[i][3]=="SI"){
                    servidor3=true;
                    servicioofrecido=Integer.parseInt(serv3.LSL[i][0]);
            }
        }
        
        for (i=0; i<=11; i++){
            if (servgen.LSG[i][2].equals(clave) && servgen.LSG[i][4]=="SI"){
                    servidor2=true;
                    servicioofrecido=Integer.parseInt(servgen.LSG[i][0]);
            }
        }
        
        for (i=0; i<=11; i++){
            if (servgen.LSG[i][2].equals(clave) && servgen.LSG[i][3]=="SI"){
                    servidor1=true;
                    servicioofrecido=Integer.parseInt(servgen.LSG[i][0]);
            }
        }
    }
    
    public MultiThread3(Socket socket){
        super("MultiThread1");
        this.socket = socket;
        Servidor3.NoClients++;
    }

    public void run() {
        try{
            PrintWriter escritor = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String lineIn, lineOut;

	    while((lineIn = entrada.readLine()) != null){
                System.out.println("Received: "+lineIn);
                escritor.flush();
                if(lineIn.equals("FIN")){
                    Servidor3.NoClients--;
                    break;
                }
                else{
                    ChecaSiOfrece(lineIn);
                    if (servidor3==true){
                        //YO ATIENDO EL SERVIVIO
                        escritor.println(Servicio(lineIn,servicioofrecido));
                        escritor.flush();
                        servidor3=false;
                    }
                    else{
                        if (servidor1==true){
                                Socket cliente = null; 
                                PrintWriter escritorn = null; 
                                String DatosEnviados = null; 
                                BufferedReader entradan =null;
                                BufferedReader DatosTeclado = new BufferedReader ( new InputStreamReader (System.in)); 
                                System.out.println ("Establecidos valores por defecto:\nEQUIPO = localhost\nPORT = 12345"); 

                                try{ 
                                    cliente = new Socket ("localhost",12345); 
                                }catch (Exception e){ 
                                    System.out.println ("Fallo : "+ e.toString()); 
                                    System.exit (0); 
                                }

                                try{ 
                                    escritorn = new PrintWriter(cliente.getOutputStream(), true);
                                    entradan=new BufferedReader(new InputStreamReader(cliente.getInputStream())); 
                                }catch (Exception e){ 
                                    System.out.println ("Fallo : "+ e.toString()); 
                                    cliente.close(); 
                                    System.exit (0); 
                                } 

                                String line;	
                                System.out.println("Conectado con el Servidor 1. Listo para enviar datos...");
                
                                escritorn.println (lineIn); //BIEN
                                line = entradan.readLine();
                                escritor.println(line);
                                escritor.flush();
                                escritorn.flush();

                                System.out.println ("Finalizada conexion con el servidor 1"); 
                                try{ 
                                    escritorn.close(); 
                                }catch (Exception e){
                                }
                                servidor1=false;
                        }
                        else{
                            if(servidor2==true){
                                Socket cliente = null; 
                                PrintWriter escritorn = null; 
                                String DatosEnviados = null; 
                                BufferedReader entradan =null;
                                BufferedReader DatosTeclado = new BufferedReader ( new InputStreamReader (System.in)); 
                                System.out.println ("Establecidos valores por defecto:\nEQUIPO = localhost\nPORT = 12346"); 

                                try{ 
                                    cliente = new Socket ("localhost",12346); 
                                }catch (Exception e){ 
                                    System.out.println ("Fallo : "+ e.toString()); 
                                    System.exit (0); 
                                }

                                try{ 
                                    escritorn = new PrintWriter(cliente.getOutputStream(), true);
                                    entradan=new BufferedReader(new InputStreamReader(cliente.getInputStream())); 
                                }catch (Exception e){ 
                                    System.out.println ("Fallo : "+ e.toString()); 
                                    cliente.close(); 
                                    System.exit (0); 
                                } 

                                String line;	
                                System.out.println("Conectado con el Servidor 2. Listo para enviar datos...");
                
                                escritorn.println (lineIn); //BIEN
                                line = entradan.readLine();
                                escritor.println(line);
                                escritor.flush();
                                escritorn.flush();

                                System.out.println ("Finalizada conexion con el servidor 2"); 
                                try{ 
                                    escritorn.close(); 
                                }catch (Exception e){
                                }
                                servidor2=false;
                            }
                            else{
                                escritor.println("El servicio que solicitaste no es válido");
                                escritor.flush();                                
                            }
                        }
                    }
                }
            }
       
            try{		
                entrada.close();
                escritor.close();
                socket.close();
            }catch(Exception e){ 
            System.out.println ("Error : " + e.toString()); 
            socket.close();
            System.exit (0); 
            }
            
        }catch (IOException e) {
            e.printStackTrace();
        }
    }

    
    public String Servicio (String cadena, int serv) throws IOException{
        String resultado=null;
        switch (serv){
            case 1:
                resultado="#B-R#"+ABinario(cadena.substring(3,cadena.length()-1))+"#";
                break;
            case 2:
                resultado="#I-R#"+Invertir(cadena.substring(3,cadena.length()-1))+"#";
                break;
            case 3:
                resultado=NumerosaLetras(cadena);
                break;
            case 4:
                resultado="#M-R#"+AMayusculas(cadena.substring(3,cadena.length()-1))+"#";
                break;
            case 5:
                resultado="#m-R#"+AMinusculas(cadena.substring(3,cadena.length()-1))+"#";
                break;
            case 6:
                resultado="#C-R#Hay "+DolarenFB()+" clientes conectados#";
                break;
            case 7:
                resultado="#E-R#"+Encriptar(cadena.substring(3,cadena.length()-1))+"#";
                break;
            case 8:
                resultado="#D-R#"+Desencriptar(cadena.substring(3,cadena.length()-1))+"#";
                break;
            case 9:
                resultado="#P-R#"+PostearenFB()+"#";
                break;
            case 10:
                resultado="A-R#"+CalidadAire()+"#";
                break;
            case 11:
                resultado="T-R#El mensaje: "+MensajeTelegram(cadena.substring(3,cadena.length()-1))+" fue enviado#";
                break;
        }
        return resultado;
    }
    
    public String MensajeTelegram(String mensaje) throws MalformedURLException, IOException{
        String urlString = "https://api.telegram.org/bot%s/sendMessage?chat_id=%s&text=%s";

        String apiToken = "857857046:AAFCLOoyuwHWDEmK6xTkZ9D-PsFlbFFdDfE";
        String chatId = "@PruebasSistemasDistribuidosFinal";
        String text = mensaje;
        urlString = String.format(urlString, apiToken, chatId, text);

        URL url = new URL(urlString);
        URLConnection conn = url.openConnection();
        StringBuilder sb = new StringBuilder();
        InputStream is = new BufferedInputStream(conn.getInputStream());
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String inputLine = "";
        while ((inputLine = br.readLine()) != null) {
            sb.append(inputLine);
        }
        String response = sb.toString();
        return mensaje;
    }
    
  public String DolarenFB() throws IOException{
      Document doc;
            doc = Jsoup.connect("https://es.fxexchangerate.com/usd/mxn.html").get();
            String titulo=doc.title();
            
            //Elements c= doc.select("em.resaltado2");
            //System.out.println(t);
            //Elements compra= doc.select("p.resalta2[style='font-size:100%;font-weight:500']");
            //String s= compra.attr("style=\"font-size:100%;font-weight:500\"");

            ///Elements venta= doc.select("p.resalta");
            //System.out.println("Compra:"+s);
            //System.out.println(compra.text()+venta.text());
            String r="Precio del dolar hoy en bancos de Mexico y tipo de cambio actual:19.3763 MXN\n";//compra.text()+"\n"+venta.text();
            HttpResponse re;
            String accessToken = "EAAlkOYBQB4QBAGUOhoyuTGoOSvwkIqTZCuvNWriERF2vzAW7PakeXzjZBUuPAlEC0zcRvZC5ipFzKzxLfDZASn9NuCORrSh1nAlZA4EdawmkxWUULElXT170Kn2FotPqjCnTdVTfPZBPW2LQdYGGWCXDxOLXaN2z4os2daefclZAhIkQE8LT3VpWfOeG7FQdTAZD";
            re = Request.Post("https://graph.facebook.com/105312894202306/feed/").bodyForm(Form.form().add("message",r).add("access_token", accessToken).build()).execute().returnResponse();
            
            System.out.println("Se ha publicado el cima y humedad con éxito");
            
  return r;
       
    }
    
    
    
    
    
    public String PostearenFB() throws IOException{
       Document doc;
        
            doc = Jsoup.connect("https://www.msn.com/es-mx/el-tiempo/pronostico/Ciudad-de-M%C3%A9xico,CDMX,M%C3%A9xico/we-city?iso=MX&el=ZfakICI4rr0%2FHb0tnBkVbg%3D%3D").timeout(100000).get();
            Elements temp= doc.select("span.current");
            //System.out.println(doc.title());
            System.out.println(temp.attr("aria-label"));
            String temperatura= temp.attr("aria-label");
            Elements h= doc.select("div.weather-info");
            Elements h1= h.select("ul");
            Elements h2= h1.select("li");
            String info= h2.text();
            
            
            String r=temperatura;
            r.concat(" Información General: ");
            r.concat(info);

            HttpResponse re;
            String accessToken = "EAAlkOYBQB4QBAGUOhoyuTGoOSvwkIqTZCuvNWriERF2vzAW7PakeXzjZBUuPAlEC0zcRvZC5ipFzKzxLfDZASn9NuCORrSh1nAlZA4EdawmkxWUULElXT170Kn2FotPqjCnTdVTfPZBPW2LQdYGGWCXDxOLXaN2z4os2daefclZAhIkQE8LT3VpWfOeG7FQdTAZD";
            re = Request.Post("https://graph.facebook.com/105312894202306/feed/").bodyForm(Form.form().add("message",r).add("access_token", accessToken).build()).execute().returnResponse();
            
            System.out.println("Se ha publicado el cima y humedad con éxito");
        return r;
              
        
    }
    
    public String CalidadAire() throws IOException{
        URL url = new URL("http://www.aire.cdmx.gob.mx/default.php");
        URLConnection uc = url.openConnection();
        uc.connect();
        int ph=0;
        BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()));
        String inputLine;
        String Calidad = "", Indice="", decodificacion="";
        while ((inputLine = in.readLine()) != null) {
            if (inputLine.contains("images/iconos-calidad-aire-home/")){
                for (int i=0; i<inputLine.length();i++){
                    if(inputLine.charAt(i)=='.')
                        ph=i;
                }
                Calidad = inputLine.substring(102,ph);
            }
            if (inputLine.contains("rengloncuatrodatoscalidadaireahora")){
                Indice = inputLine.substring(150,414);
                try {
                    decodificacion = java.net.URLDecoder.decode(Indice, StandardCharsets.UTF_8.name());
                } catch (UnsupportedEncodingException e) {
                
                }
                for (int i=0; i<decodificacion.length();i++){
                    if(decodificacion.charAt(i)=='w')
                        ph=i;
                }
                Indice = decodificacion.substring(73,75);
            }
        }
        in.close();
        return "La calidad del aire en la CDMX es: "+Calidad+", el índice de PM10 es: "+Indice+"/500";
    }
        
    
    public String AMayusculas (String texto){
        if (texto.length()==0)
            return "NO INTRODUCISTE TEXTO ALGUNO";
        else
            return texto.toUpperCase();
    }
    
    public String AMinusculas (String texto){
        if (texto.length()==0)
            return "NO INTRODUCISTE TEXTO ALGUNO";
        else
            return texto.toLowerCase();
    }
    
    public String ABinario(String numero){
        String nb="";
        if (numero.length()==0)
            nb="NO INSERTASTE NÚMERO ALGUNO PARA CONVERTIR";
        else{
            for (int j=0; j<numero.length(); j++){
                if (numero.charAt(j)<48 || numero.charAt(j)>57)
                    return "INTRODUCISTE AL MENOS UN CARACTER NO NUMÉRICO";
            }
            int resto, i;
            int num = Integer.parseInt(numero);
            ArrayList<String> binario = new ArrayList<String>();
            do{
                resto=num%2;
                num=num/2;
                binario.add(0, Integer.toString(resto));
            }while(num>0);

            for(i=0; i<binario.size(); i++)
                nb+=binario.get(i);
        }
        return nb;
    }
    
    public String Invertir(String cadena){
        String ncadena="";
        if (cadena.length()==0)
            ncadena="NO INSERTASTE TEXTO ALGUNO";
        else{
            for (int j=cadena.length()-1; j>=0; j--)
                ncadena=ncadena+cadena.charAt(j);
        }
        return ncadena;
    }
    
    public static String Encriptar (String texto){
        String llave = "GabrielRodriguez";
        String textoencriptado = "";
        
        if (texto.length()==0)
            textoencriptado="NO INSERTASTE TEXTO ALGUNO PARA ENCRIPTAR";
        else{
            try {
                MessageDigest md = MessageDigest.getInstance("MD5");
                byte[] digestOfPassword = md.digest(llave.getBytes("utf-8"));
                byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
 
                SecretKey key = new SecretKeySpec(keyBytes, "DESede");
                Cipher cipher = Cipher.getInstance("DESede");
                cipher.init(Cipher.ENCRYPT_MODE, key);
 
                byte[] plainTextBytes = texto.getBytes("utf-8");
                byte[] buf = cipher.doFinal(plainTextBytes);
                byte[] base64Bytes = Base64.encodeBase64(buf);
                textoencriptado = new String(base64Bytes);
 
            }catch (Exception ex) {
            }
        }
        return textoencriptado;
    }
    
    public static String Desencriptar (String texto)/* throws Exception */{
        String llave = "GabrielRodriguez";
        String textodesencriptado = "";
        
        if (texto.length()==0)
            textodesencriptado="NO INSERTASTE TEXTO ALGUNO PARA DESENCRIPTAR";
        else{
            try {
                byte[] message = Base64.decodeBase64(texto.getBytes("utf-8"));
                MessageDigest md = MessageDigest.getInstance("MD5");
                byte[] digestOfPassword = md.digest(llave.getBytes("utf-8"));
                byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
                SecretKey key = new SecretKeySpec(keyBytes, "DESede");

                Cipher decipher = Cipher.getInstance("DESede");
                decipher.init(Cipher.DECRYPT_MODE, key);

                byte[] plainText = decipher.doFinal(message);

                textodesencriptado = new String(plainText, "UTF-8");

            }catch (Exception ex) {
            }
        }
        return textodesencriptado;
    }
    
    public String NumerosaLetras (String cadena){
        String lineIn = cadena;
        String resultado="";
        for (int j=3; j<=cadena.length()-2; j++){
            if (cadena.charAt(j)<48 || cadena.charAt(j)>57)
                return "#L-R#INTRODUCISTE AL MENOS UN CARACTER NO NUMÉRICO#";
        }
        switch(cadena.length()-4){
            case 0:
                resultado="#L-R#NO INTRODUCISTE ALGÚN NÚMERO#";
                break;
            case 1: //unidades
                if (lineIn.charAt(3)==48)
                    resultado="#L-R#CERO#";
                else
                    resultado=(TextoUnidades(lineIn.substring(3,4)));
                break;
            case 2: //decenas
                if (lineIn.charAt(3)<51)//menora30
                    resultado=("#L-R#"+TextoDecenas029(lineIn.substring(3,5))+"#");
                else
                    resultado="#L-R#"+TextoDecenas(lineIn.substring(3,4))+" Y"+TextoUnidades(lineIn.substring(4,5))+"#";
                break;
            case 3: //centenas
                if (lineIn.charAt(3)==49 && lineIn.charAt(4)==48 && lineIn.charAt(5)==48)
                    resultado="#L-R#CIEN#";
                else{
                    if (lineIn.charAt(3)==49){
                        if (lineIn.charAt(4)<51)
                            resultado="#L-R#CIENTO "+TextoDecenas029(lineIn.substring(4,6))+"#";
                        else
                            resultado="#L-R#CIENTO "+TextoDecenas(lineIn.substring(4,5))+"Y "+TextoUnidades(lineIn.substring(5,6))+"#";
                    }
                    else{
                        if (lineIn.charAt(3)>49 && lineIn.charAt(4)<51)
                            resultado="#L-R#"+TextoCentenas(lineIn.substring(3,4))+TextoDecenas029(lineIn.substring(4,6))+"#";
                        else
                            resultado="#L-R#"+TextoCentenas(lineIn.substring(3,4))+TextoDecenas(lineIn.substring(4,5))+"Y "+TextoUnidades(lineIn.substring(5,6))+"#";
                        }
                    }
                break;
            case 4:
                if (lineIn.charAt(3)==49){
                    if (lineIn.charAt(4)==49 && lineIn.charAt(5)==48 && lineIn.charAt(6)==48)
                        resultado="#L-R#MIL CIEN#";
                    else{
                        if (lineIn.charAt(4)==49){
                            if (lineIn.charAt(5)<51)
                                resultado="#L-R#MIL CIENTO "+TextoDecenas029(lineIn.substring(5,7))+"#";
                            else
                                resultado=("#L-R#MIL CIENTO "+TextoDecenas(lineIn.substring(5,6))+"Y "+TextoUnidades(lineIn.substring(6,7))+"#");
                        }
                        else{
                            if (lineIn.charAt(4)>49 && lineIn.charAt(5)<51)
                                resultado="#L-R#MIL "+TextoCentenas(lineIn.substring(4,5))+TextoDecenas029(lineIn.substring(5,7))+"#";
                            else
                                resultado="#L-R#MIL "+TextoCentenas(lineIn.substring(4,5))+TextoDecenas(lineIn.substring(5,6))+"Y "+TextoUnidades(lineIn.substring(6,7))+"#";
                            }
                        }
                }
                else{
                    if (lineIn.charAt(4)==49 && lineIn.charAt(5)==48 && lineIn.charAt(6)==48)
                        resultado="#L-R#"+TextoUnidades(lineIn.substring(3,4))+" MIL CIEN#";
                    else{
                        if (lineIn.charAt(4)==49){
                            if (lineIn.charAt(5)<51)
                                resultado="#L-R#"+TextoUnidades(lineIn.substring(3,4))+" MIL CIENTO "+TextoDecenas029(lineIn.substring(5,7))+"#";
                            else
                                resultado=("#L-R#"+TextoUnidades(lineIn.substring(3,4))+" MIL CIENTO "+TextoDecenas(lineIn.substring(5,6))+"Y "+TextoUnidades(lineIn.substring(6,7))+"#");
                        }
                        else{
                            if (lineIn.charAt(4)>49 && lineIn.charAt(5)<51)
                                resultado="#L-R#"+TextoUnidades(lineIn.substring(3,4))+" MIL "+TextoCentenas(lineIn.substring(4,5))+TextoDecenas029(lineIn.substring(5,7))+"#";
                            else
                                resultado="#L-R#"+TextoUnidades(lineIn.substring(3,4))+" MIL "+TextoCentenas(lineIn.substring(4,5))+TextoDecenas(lineIn.substring(5,6))+"Y "+TextoUnidades(lineIn.substring(6,7))+"#";
                            }
                        }                    
                    }
                break;
        }
        return resultado;
    }
    
    public String TextoUnidades(String numero){
        String t="";
        int num = Integer.parseInt(numero);
        switch(num){
            case 0:
                t="";
                break;
            case 1:
                t="UNO";
                break;
            case 2:
                t="DOS";
                break;
            case 3:
                t="TRES";
                break;
            case 4:
                t="CUATRO";
                break;    
            case 5:
                t="CINCO";
                break;
            case 6:
                t="SEIS";
                break;
            case 7:
                t="SIETE";
                break;
            case 8:
                t="OCHO";
                break;
            case 9:
                t="NUEVE";
                break;    
        }
        return t;
    }    
   
    public String TextoDecenas029(String numero){
        String t="";
        int num1 = Integer.parseInt(numero.substring(0,1));
        int num2 = Integer.parseInt(numero.substring(1,2));
        switch(num1){
            case 0:
                switch(num2){
                    case 0:
                        t="";
                        break;
                    case 1:
                        t="UNO";
                        break;
                    case 2:
                        t="DOS";
                        break;
                    case 3:
                        t="TRES";
                        break;
                    case 4:
                        t="CUATRO";
                        break;    
                    case 5:
                        t="CINCO";
                        break;
                    case 6:
                        t="SEIS";
                        break;
                    case 7:
                        t="SIETE";
                        break;
                    case 8:
                        t="OCHO";
                        break;
                    case 9:
                        t="NUEVE";
                        break;    
                }
                break;
            case 1:
                switch(num2){
                    case 0:
                        t="DIEZ";
                        break;
                    case 1:
                        t="ONCE";
                        break;
                    case 2:
                        t="DOCE";
                        break;
                    case 3:
                        t="TRECE";
                        break;
                    case 4:
                        t="CATORCE";
                        break;    
                    case 5:
                        t="QUINCE";
                        break;
                    case 6:
                        t="DIECISEIS";
                        break;
                    case 7:
                        t="DIECISIETE";
                        break;
                    case 8:
                        t="DIECIOCHO";
                        break;
                    case 9:
                        t="DIECINUEVE";
                        break;    
                }
                break;
            case 2:
                switch(num2){
                    case 0:
                        t="VEINTE";
                        break;
                    case 1:
                        t="VEINTIUNO";
                        break;
                    case 2:
                        t="VEINTIDOS";
                        break;
                    case 3:
                        t="VEINTITRES";
                        break;
                    case 4:
                        t="VEINTICUATRO";
                        break;    
                    case 5:
                        t="VEINTICINCO";
                        break;
                    case 6:
                        t="VEINTISEIS";
                        break;
                    case 7:
                        t="VEINTISIETE";
                        break;
                    case 8:
                        t="VEINTIOCHO";
                        break;
                    case 9:
                        t="VEINTINUEVE";
                        break;    
                }
                break;   
        }
        return t;
    }    
    
    public String TextoDecenas(String numero){
        String t="";
        int num = Integer.parseInt(numero);
        switch(num){
            case 0:
                t="";
                break;
            case 1:
                t="";
                break;
            case 2:
                t="VEINTI";
                break;
            case 3:
                t="TREINTA ";
                break;
            case 4:
                t="CUARENTA ";
                break;    
            case 5:
                t="CINCUENTA ";
                break;
            case 6:
                t="SESENTA ";
                break;
            case 7:
                t="SETENTA ";
                break;
            case 8:
                t="OCHENTA ";
                break;
            case 9:
                t="NOVENTA ";
                break;    
        }
        return t;
    }   
   
    
    public String TextoCentenas(String numero){
        String t="";
        int num = Integer.parseInt(numero);
        switch(num){
            case 0:
                t="";
                break;
            case 1:
                t="";
                break;
            case 2:
                t="DOSCIENTOS ";
                break;
            case 3:
                t="TRESCIENTOS ";
                break;
            case 4:
                t="CUATROCIENTOS ";
                break;    
            case 5:
                t="QUINIENTOS ";
                break;
            case 6:
                t="SEISCIENTOS ";
                break;
            case 7:
                t="SETECIENTOS ";
                break;
            case 8:
                t="OCHOCIENTOS ";
                break;
            case 9:
                t="NOVECIENTOS ";
                break;    
        }
        return t;
    }
    
} 
