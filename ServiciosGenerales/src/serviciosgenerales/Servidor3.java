import java.net.*;
import java.io.*;

public class Servidor3{
    static int NoClients=0;
    
    String [][] LSL = {{"Número del Servicio","Nombre del Servicio","Clave","Disponibilidad"}
            ,{"1","A Binario","#B##","SI"},{"2","Invertir Cadena","#I##","NO"}
            ,{"3","Números a Letras","#L##","NO"},{"4","A Mayúsculas","#M##","SI"}
            ,{"5","A Minúsculas","#m##","SI"},{"6","Cuántos","#C##","SI"}
            ,{"7","Encriptar","#E##","NO"},{"8","Desencriptar","#D##","NO"}
            ,{"9","Postear en Facebook","#P##","SI"} ,{"10","Calidad del Aire","#A##","SI"}
            ,{"11","Mensaje a Telegram","#T##","NO"}};
    
    public static void main (String[] argumentos)throws IOException{
	ServerSocket socketServidor = null;
	Socket socketCliente = null;
        
	try{
            socketServidor = new ServerSocket (12347);
	}catch (Exception e){
            System.out.println ("Error : "+ e.toString());
            System.exit (0);
	}

	System.out.println ("Server started... (Socket TCP)");
	int enproceso=1;
	while(enproceso==1){
            try{
                socketCliente = socketServidor.accept();
                MultiThread3 controlThread=new MultiThread3(socketCliente);
                controlThread.start();
            }catch (Exception e){
	    	System.out.println ("Error : " + e.toString());
		socketServidor.close();
		System.exit (0);
            }
	}
        
	System.out.println("Finalizando Servidor 3...");
   }
}
