import java.net.*;
import java.io.*;

public class Servidor2{
    static int NoClients=0;
    
    String [][] LSL = {{"Número del Servicio","Nombre del Servicio","Clave","Disponibilidad"}
            ,{"1","A Binario","#B##","NO"},{"2","Invertir Cadena","#I##","SI"}
            ,{"3","Números a Letras","#L##","NO"},{"4","A Mayúsculas","#M##","SI"}
            ,{"5","A Minúsculas","#m##","NO"},{"6","Cuántos","#C##","SI"}
            ,{"7","Encriptar","#E##","SI"},{"8","Desencriptar","#D##","SI"}
            ,{"9","Postear en Facebook","#P##","NO"},{"10","Calidad del Aire","#A##","NO"}
            ,{"11","Mensaje a Telegram","#T##","SI"}};
    
    
    public static void main (String[] argumentos)throws IOException{
	ServerSocket socketServidor = null;
	Socket socketCliente = null;
        
	try{
            socketServidor = new ServerSocket (12346);
	}catch (Exception e){
            System.out.println ("Error : "+ e.toString());
            System.exit (0);
	}

	System.out.println ("Server started... (Socket TCP)");
	int enproceso=1;
	while(enproceso==1){
            try{
                socketCliente = socketServidor.accept();
                MultiThread2 controlThread=new MultiThread2(socketCliente);
                controlThread.start();
            }catch (Exception e){
	    	System.out.println ("Error : " + e.toString());
		socketServidor.close();
		System.exit (0);
            }
	}
        
	System.out.println("Finalizando Servidor 2...");
   }
}