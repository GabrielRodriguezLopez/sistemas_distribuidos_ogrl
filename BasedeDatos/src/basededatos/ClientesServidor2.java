import java.net.*; 
import java.io.*; 
import java.util.Scanner;

public class ClientesServidor2{ 
    public static void main (String[] argumentos)throws IOException{ 
	Socket cliente = null; 
	PrintWriter escritor = null; 
	String DatosEnviados = null; 
	BufferedReader entrada=null;
		
	String maquina; 
	int puerto; 
	BufferedReader DatosTeclado = new BufferedReader ( new InputStreamReader (System.in)); 

	if (argumentos.length != 2){ 
            maquina = "localhost"; 
            puerto = 12346; 
            System.out.println ("Establecidos valores por defecto:\nEQUIPO = localhost\nPORT = 12346"); 
	} 
	else{ 
            maquina = argumentos[0]; 
            Integer pasarela = new Integer (argumentos[1]); 
            puerto = pasarela.parseInt(pasarela.toString()); 
            System.out.println ("Conectado a " + maquina + " en puerto: " + puerto); 
	}
        
	try{ 
            cliente = new Socket (maquina,puerto); 
	}catch (Exception e){ 
            System.out.println ("Fallo : "+ e.toString()); 
            System.exit (0); 
	}
		
        try{ 
            escritor = new PrintWriter(cliente.getOutputStream(), true);
            entrada=new BufferedReader(new InputStreamReader(cliente.getInputStream())); 
	}catch (Exception e){ 
            System.out.println ("Fallo : "+ e.toString()); 
            cliente.close(); 
            System.exit (0); 
        } 
	
        String line;	
	System.out.println("Conectado con el Servidor 2. Listo para enviar datos...");
	String usuario="", password="";
        int valido=0;
        Scanner teclado = new Scanner(System.in);
        do{
            System.out.println("En caso de haber olvidado tu usuario o contraseña, teclea FIN en ambos campos para cerrar conexión.");                        
            System.out.println("Introduce tu nombre de usuario: ");
            usuario=teclado.nextLine();
            System.out.println("Introduce tu contraseña: ");
            password=teclado.nextLine();
            valido = ValidarAcceso(usuario,password);
        }while(valido==0);     
                
	do{
            if (usuario.equals("FIN") && password.equals("FIN"))
                DatosEnviados = "FIN";
            else{
                System.out.println("\nBIENVENIDO AL SISTEMA, INGRESE LA CONSULTA SIGUIENTE EL FORMATO:\n#1#CONSULTA#\n ");
                DatosEnviados = DatosTeclado.readLine(); 
                escritor.println (DatosEnviados); 
                line = entrada.readLine();
                System.out.println(line);
                escritor.flush();
            }
        }while (!DatosEnviados.equals("FIN")); 
	
	System.out.println ("Finalizada conexion con el servidor 1"); 
	try{ 
            escritor.close(); 
	}catch (Exception e){}
	}
        
    public static int ValidarAcceso(String usuario, String password) throws FileNotFoundException, IOException{
        int resultado = 0;
        String cadena="";
        String u="usuario=\""+usuario+"\"", p="password=\""+password+"\"";
        FileReader f = new FileReader("C:\\Users\\sandy\\Documents\\UPIITA_9_Semestre\\Sistemas Distribuidos\\ServiciosGenerales\\src\\serviciosgenerales\\Usuarios.txt");
        BufferedReader b = new BufferedReader(f);
        while((cadena = b.readLine())!=null) {
            if ((cadena.contains(u) && cadena.contains(p))){
                resultado=1;
                break;
            }
        }
        b.close();
        return resultado;
    }
} 
