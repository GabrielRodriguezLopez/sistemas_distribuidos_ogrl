import java.net.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.security.MessageDigest;
import java.util.Arrays;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;
//import com.restfb.*;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;
import org.apache.http.*;
import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import java.sql.Connection;
import java.sql.*;
import java.awt.HeadlessException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;


public class MultiThread3 extends Thread{
    private Socket socket = null;
    boolean servidor1 = false, servidor2 = false, servidor3 = false;
    int servicioofrecido=0;
    
    public void ChecaSiOfrece(String entrada){
      int i=0;
        char [] NumB= entrada.toCharArray();
        //String clave="";
        //clave=entrada.substring(0,3)+entrada.substring(entrada.length()-1,entrada.length());
        //ServiciosGenerales servgen = new ServiciosGenerales();
        //Servidor2 serv2 = new Servidor2();
        
       if (NumB[1]=='1'){
                    servidor1=true;
                    //servicioofrecido=Integer.parseInt(serv1.LSL[i][0]);
            }
            if (NumB[1]=='2'){
                    servidor2=true;
                    //servicioofrecido=Integer.parseInt(servgen.LSG[i][0]);
            }
    
            if (NumB[1]=='3'){
                    servidor3=true;
                    //servicioofrecido=Integer.parseInt(servgen.LSG[i][0]);
            }
    }
    
    public MultiThread3(Socket socket){
        super("MultiThread1");
        this.socket = socket;
        Servidor3.NoClients++;
    }

    public void run() {
        try{
            PrintWriter escritor = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String lineIn, lineOut;

	    while((lineIn = entrada.readLine()) != null){
                System.out.println("Received: "+lineIn);
                escritor.flush();
                if(lineIn.equals("FIN")){
                    Servidor3.NoClients--;
                    break;
                }
                else{
                    ChecaSiOfrece(lineIn);
                    if (servidor3==true){
                        //YO ATIENDO EL SERVIVIO
                        escritor.println(Servicio(lineIn));
                        escritor.flush();
                        servidor3=false;
                    }
                    else{
                        if (servidor1==true){
                                Socket cliente = null; 
                                PrintWriter escritorn = null; 
                                String DatosEnviados = null; 
                                BufferedReader entradan =null;
                                BufferedReader DatosTeclado = new BufferedReader ( new InputStreamReader (System.in)); 
                                System.out.println ("Establecidos valores por defecto:\nEQUIPO = localhost\nPORT = 12345"); 

                                try{ 
                                    cliente = new Socket ("localhost",12345); 
                                }catch (Exception e){ 
                                    System.out.println ("Fallo : "+ e.toString()); 
                                    System.exit (0); 
                                }

                                try{ 
                                    escritorn = new PrintWriter(cliente.getOutputStream(), true);
                                    entradan=new BufferedReader(new InputStreamReader(cliente.getInputStream())); 
                                }catch (Exception e){ 
                                    System.out.println ("Fallo : "+ e.toString()); 
                                    cliente.close(); 
                                    System.exit (0); 
                                } 

                                String line;	
                                System.out.println("Conectado con el Servidor 1. Listo para enviar datos...");
                
                                escritorn.println (lineIn); //BIEN
                                line = entradan.readLine();
                                escritor.println(line);
                                escritor.flush();
                                escritorn.flush();

                                System.out.println ("Finalizada conexion con el servidor 1"); 
                                try{ 
                                    escritorn.close(); 
                                }catch (Exception e){
                                }
                                servidor1=false;
                        }
                        else{
                            if(servidor2==true){
                                Socket cliente = null; 
                                PrintWriter escritorn = null; 
                                String DatosEnviados = null; 
                                BufferedReader entradan =null;
                                BufferedReader DatosTeclado = new BufferedReader ( new InputStreamReader (System.in)); 
                                System.out.println ("Establecidos valores por defecto:\nEQUIPO = localhost\nPORT = 12346"); 

                                try{ 
                                    cliente = new Socket ("localhost",12346); 
                                }catch (Exception e){ 
                                    System.out.println ("Fallo : "+ e.toString()); 
                                    System.exit (0); 
                                }

                                try{ 
                                    escritorn = new PrintWriter(cliente.getOutputStream(), true);
                                    entradan=new BufferedReader(new InputStreamReader(cliente.getInputStream())); 
                                }catch (Exception e){ 
                                    System.out.println ("Fallo : "+ e.toString()); 
                                    cliente.close(); 
                                    System.exit (0); 
                                } 

                                String line;	
                                System.out.println("Conectado con el Servidor 2. Listo para enviar datos...");
                
                                escritorn.println (lineIn); //BIEN
                                line = entradan.readLine();
                                escritor.println(line);
                                escritor.flush();
                                escritorn.flush();

                                System.out.println ("Finalizada conexion con el servidor 2"); 
                                try{ 
                                    escritorn.close(); 
                                }catch (Exception e){
                                }
                                servidor2=false;
                            }
                            else{
                                escritor.println("El servicio que solicitaste no es válido");
                                escritor.flush();                                
                            }
                        }
                    }
                }
            }
       
            try{		
                entrada.close();
                escritor.close();
                socket.close();
            }catch(Exception e){ 
            System.out.println ("Error : " + e.toString()); 
            socket.close();
            System.exit (0); 
            }
            
        }catch (IOException e) {
            e.printStackTrace();
        }
    }

    
    public String Servicio (String cadena) throws IOException{
         String resultado=null;
        char [] DR= cadena.toCharArray();
        int e=3;
        String consulta="";
                                    do{
                                        consulta=consulta+DR[e];
                                        e++;                                  
                                    }while(DR[e]!='#');
        
        //Empieza lo de MySQL
            try
    {
      // create our mysql database connection
      String myDriver = "com.mysql.jdbc.Driver";
      String myUrl = "jdbc:mysql://localhost:3306/tiendas";
      Class.forName(myDriver);
      Connection conn = DriverManager.getConnection(myUrl, "root", "12345678");
      
      // our SQL SELECT query. 
      // if you only need a few columns, specify them by name instead of using "*"
      String query = consulta;

      // create the java statement
      Statement st = conn.createStatement();
      
      // execute the query, and get a java resultset
      ResultSet rs = st.executeQuery(query);
      
      // iterate through the java resultset
      while (rs.next())
      {
        int id = rs.getInt("productID");
        String firstName = rs.getString("productCode");
        String lastName = rs.getString("name");
        int dateCreated = rs.getInt("quantity");
        float isAdmin = rs.getFloat("price");
    
        
        // print the results
       resultado="Servidor3 Responde: #ID: "+id+" Código: "+firstName+" Nombre: "+lastName+"Cantidad: "+dateCreated+"Precio:"+isAdmin+"#"; 
//System.out.format("%s, %s, %s, %s, %s, %s\n", id, firstName, lastName, dateCreated, isAdmin);
      }
      st.close();
    }
    catch (Exception ex)
    {
      System.err.println("Got an exception! ");
      System.err.println(ex.getMessage());
      resultado=ex.getMessage();
    }                           
        //Finaliza lo de MySQL
        
        return resultado;
    }
    
    
} 